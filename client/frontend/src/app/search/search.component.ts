import { Component, OnInit } from '@angular/core';
import { ApiService } from '../search/api.service';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-search',
  template: `
  <section class="filter-wrapper">
 <div class="keyword-wrapper">
<input [formControl]="queryField" (input)="ngOnInit($event.target.value)" type="text" id="keyword" placeholder="search for ..." autofocus/>
  </div>
  <ul class="list-group">
  <li class="list-group-item">{{results.name}}</li>
  <li class="list-group-item">{{results.followers}}</li>
  <li class="list-group-item">{{results.following}}</li>
  <li class="list-group-item">{{results.updated_at}}</li>
  <li class="list-group-item">{{results.created_at}}</li>
</ul>
</section>
  `,
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  results: any[] = [];
  queryField: FormControl = new FormControl();
  constructor(private _apiService: ApiService) { }

  ngOnInit() {

    this.queryField.valueChanges
      .debounceTime(200)
      .distinctUntilChanged()
      .switchMap((query) => this._apiService.search(query))
      .subscribe(result => {

        if (result.status === 400) {
          return;
        } else {
          this.results = result.json()
        }
      });

    
  }
}